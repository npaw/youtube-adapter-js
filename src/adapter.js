/* global YT */
var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Youtube = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getCurrentTime()
  },

  getPlayrate: function () {
    return this.player.getPlaybackRate()
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (!this.plugin.getIsLive()) {
      ret = this.player.getDuration()
    }
    return ret
  },

  /** Override to return rendition */
  getRendition: function () {
    return this.player.getPlaybackQuality()
  },

  /** Override to return title */
  getTitle: function () {
    var ret = 'unknown'
    if (this.player.getVideoData()) {
      ret = this.player.getVideoData().title
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.getVideoUrl()
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Youtube'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor
    this.monitorPlayhead(true, true)

    // Register listeners
    this.references = {
      onStateChange: this.stateChangeListener.bind(this),
      onError: this.errorListener.bind(this)
    }
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    this.monitor.stop()

    // unregister listeners
    if (this.tag && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  stateChangeListener: function (event) {
    switch (event.data) {
      case YT.PlayerState.UNSTARTED:
      case YT.PlayerState.BUFFERING:
        if (this.flags.isPaused) {
          this.fireSeekBegin()
        }
        if (!this.blocked) {
          if (this.previousvideo !== this.getResource()) {
            this.fireStart() // first buffer will trigger /start
          }
        }
        break
      case YT.PlayerState.ENDED:
        this.fireStop()
        break
      case YT.PlayerState.PLAYING:
        if (!this.flags.isJoined) {
          this.fireStart()
          this.fireJoin()
          this.previousvideo = this.getResource()
          this.monitor.skipNextTick()
        }
        this.fireSeekEnd()
        this.fireResume()
        break
      case YT.PlayerState.PAUSED:
        this.firePause()
        break
    }
  },

  errorListener: function (event) {
    if (this.blocked) return null
    var error = {
      2: 'Invalid ID',
      5: 'HTML5 content error',
      100: 'Video not found',
      101: 'Not allowed to play by owner'
    }

    var code = event.data
    if (code === 150) {
      // Error 150 is the same as 101 as stated in the documentation
      code = 101
    }
    if (error[code] !== undefined) {
      this.fireError(code, error[code])
    } else {
      this.fireError(code, 'Unknown error')
    }
    if (code === 2 || code === 5 || code === 100 || code === 101) {
      this.fireStop()
      if (!this.isStarted && !this.isInited) this.blocked = true
    }
  }
})

module.exports = youbora.adapters.Youtube
